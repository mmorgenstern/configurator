package configurator

import (
	"io/fs"
	"os"
	"path"
	"testing"
)

var DemoValue = "Hello World!"
var DemoConfigPath = path.Join(os.TempDir(), "configurator_tests", "configs")
var DemoConfigFileName = "demo.config.json"
var DemoConfigFileMode = fs.FileMode(0755)

type DemoConfig struct {
	Value string `json:"value"`
}

func (d DemoConfig) GetLocation() string {
	return path.Join(DemoConfigPath, DemoConfigFileName)
}

func (d DemoConfig) GetFileMode() fs.FileMode {
	return DemoConfigFileMode
}

func TestNewConfigWriter(t *testing.T) {
	w := NewConfigWriter()
	if w == nil {
		t.Error("no instance of ConfigWriter get from NewConfigWriter()")
	}
}

func TestConfigWriter_Write(t *testing.T) {
	cfg := DemoConfig{Value: DemoValue}
	w := NewConfigWriter()
	err := w.Write(cfg)
	if err != nil {
		t.Error(err)
	}
	r := NewConfigReader()
	cfg.Value = "other"
	err = r.Read(&cfg)
	if err != nil {
		t.Error(err)
	}
	if cfg.Value != DemoValue {
		t.Errorf("expect config value %v but get %v", DemoValue, cfg.Value)
	}
}
