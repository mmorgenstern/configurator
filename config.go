package configurator

import "io/fs"

type IConfig interface {
	GetLocation() string
	GetFileMode() fs.FileMode
}

type IConfigWriter interface {
	Write(o IConfig) error
}

type IConfigReader interface {
	Read(o IConfig) error
}
