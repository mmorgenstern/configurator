package configurator

import (
	jsoniter "github.com/json-iterator/go"
	"io/ioutil"
	"os"
	"path"
)

type ConfigWriter struct {}

func NewConfigWriter() *ConfigWriter {
	return &ConfigWriter{}
}

func (w *ConfigWriter) Write(o IConfig) error {
	str, err := jsoniter.Marshal(o)
	if err != nil {
		return err
	}
	d := path.Dir(o.GetLocation())
	err = os.MkdirAll(d, o.GetFileMode())
	if err != nil {
		return err
	}
	return ioutil.WriteFile(o.GetLocation(), str, o.GetFileMode())
}
