# Configurator

Saves Golang Structs into Filesystem.

### Install

```bash
go get gitlab.com/mmorgenstern/configurator
```

### Usage

```go
package main

import (
	"fmt"
	"gitlab.com/mmorgenstern/configurator"
	"io/fs"
	"os"
	"path"
)

// define the structure of the Configuration Object
type Config struct {
	Name string `json:"name"`
}

// Implement the GetLocation Function from the IConfig Interface that returns the
// Path of the Config File
func (c *Config) GetLocation() string {
	return path.Join(os.TempDir(), "testprogram", "config.json")
}

// Implement the GetFileMode Function to define the Rights that saved with the File
func (c *Config) GetFileMode() fs.FileMode {
	return 0755
}

func main() {
	// create a Instance of the Configuration you want
	cfg := Config{Name: "test"}
	// get a Config Writer to write the Config
	w := configurator.NewConfigWriter()
	// get a Config Reader to read the written Config
	r := configurator.NewConfigReader()

	// write the instance of Config
	err := w.Write(&cfg)
	if err != nil {
		panic(err)
	}
	// read the Config from Filesystem into the given Reference Instance
	err = r.Read(&cfg)
	if err != nil {
		panic(err)
	}
	println(fmt.Sprintf("%v", cfg))
}
```