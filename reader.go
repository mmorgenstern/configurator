package configurator

import (
	jsoniter "github.com/json-iterator/go"
	"io/ioutil"
	"os"
	"path"
)

type ConfigReader struct {}

func NewConfigReader() *ConfigReader {
	return &ConfigReader{}
}

func (r *ConfigReader) Read(o IConfig) error {
	str, err := ioutil.ReadFile(o.GetLocation())
	if err != nil {
		return err
	}
	err = os.MkdirAll(path.Dir(o.GetLocation()), o.GetFileMode())
	if err != nil {
		return err
	}
	return jsoniter.Unmarshal(str, &o)
}
